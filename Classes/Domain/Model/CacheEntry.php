<?php
declare(strict_types=1);
namespace Pixelant\Webkitpdf\Domain\Model;

use TYPO3\CMS\Extbase\DomainObject\AbstractEntity;

/**
 * Class CacheEntry
 * @package Pixelant\Webkitpdf
 */
class CacheEntry extends AbstractEntity
{
    /**
     * MD5 of url of page
     *
     * @var string
     */
    protected $urlHash = '';

    /**
     * Cached file name
     *
     * @var string
     */
    protected $filePath = '';

    /**
     * Creation date
     *
     * @var int
     */
    protected $crdate;

    /**
     * @return string
     */
    public function getUrlHash(): string
    {
        return $this->urlHash;
    }

    /**
     * @param string $urlHash
     */
    public function setUrlHash(string $urlHash)
    {
        $this->urlHash = $urlHash;
    }

    /**
     * @return string
     */
    public function getFilePath(): string
    {
        return $this->filePath;
    }

    /**
     * @param string $filePath
     */
    public function setFilePath(string $filePath)
    {
        $this->filePath = $filePath;
    }

    /**
     * @return int
     */
    public function getCrdate(): int
    {
        return $this->crdate;
    }

    /**
     * @param int $crdate
     */
    public function setCrdate(int $crdate)
    {
        $this->crdate = $crdate;
    }
}
