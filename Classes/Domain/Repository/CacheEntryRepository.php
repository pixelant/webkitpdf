<?php

namespace Pixelant\Webkitpdf\Domain\Repository;

use Pixelant\Webkitpdf\Domain\Model\CacheEntry;
use TYPO3\CMS\Core\Database\Connection;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Persistence\Repository;

/**
 * Class CacheEntryRepository
 * @package Pixelant\Webkitpdf\Domain\Repository
 */
class CacheEntryRepository extends Repository
{
    /**
     * Get cache
     *
     * @param string $urlHash
     * @return CacheEntry|object|null
     */
    public function findCacheByUrlHash($urlHash)
    {
        $query = $this->createQuery();

        $query->getQuerySettings()
            ->setRespectSysLanguage(false)
            ->setRespectStoragePage(false);

        $query->matching(
            $query->like('urlHash', $urlHash)
        );

        $query->setLimit(1);

        return $query->execute()->getFirst();
    }

    /**
     * Delete record from DB
     * Use this method, otherwise we will keep all cache with flag deleted=1
     *
     * @param CacheEntry|int $cacheEntry
     */
    public function fullRemoveFromDB($cacheEntry)
    {
        GeneralUtility::makeInstance(ConnectionPool::class)
            ->getConnectionForTable('tx_webkitpdf_domain_model_cacheentry')
            ->delete(
                'tx_webkitpdf_domain_model_cacheentry',
                [
                    'uid' => is_object($cacheEntry) ? $cacheEntry->getUid() : (int)$cacheEntry
                ],
                [
                    Connection::PARAM_INT
                ]
            );

        if (file_exists($cacheEntry->getFilePath())) {
            unlink($cacheEntry->getFilePath());
        }
    }
}
