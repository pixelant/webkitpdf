<?php
declare(strict_types=1);

namespace Pixelant\Webkitpdf\Hooks;

use TYPO3\CMS\Core\Database\Connection;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Database\Query\QueryBuilder;
use TYPO3\CMS\Core\DataHandling\DataHandler;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Class ClearCacheHook
 * @package Pixelant\Webkitpdf\Hooks
 */
class ClearCacheHook
{
    /**
     * Remove outdate entries on clear cache
     *
     * @param array $params
     * @param DataHandler $pObj
     */
    public function clearCacheCmd(
        array $params,
        /** @noinspection PhpUnusedParameterInspection */ DataHandler $pObj
    ) {
        if (GeneralUtility::inList('all,pages', $params['cacheCmd'])) {
            /** @var QueryBuilder $queryBuilder */
            $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable(
                'tx_webkitpdf_domain_model_cacheentry'
            );
            $queryBuilder->getRestrictions()->removeAll();

            $rawCache = $queryBuilder
                ->select('file_path')
                ->from('tx_webkitpdf_domain_model_cacheentry')
                ->execute();

            while ($filePath = $rawCache->fetchColumn(0)) {
                if (file_exists($filePath)) {
                    unlink($filePath);
                }
            }

            GeneralUtility::makeInstance(ConnectionPool::class)
                ->getConnectionForTable('tx_webkitpdf_domain_model_cacheentry')
                ->truncate('tx_webkitpdf_domain_model_cacheentry');
        }
    }
}
