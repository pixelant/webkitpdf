<?php
declare(strict_types=1);

namespace Pixelant\Webkitpdf\Utility;

use TYPO3\CMS\Core\Configuration\ExtensionConfiguration;
use TYPO3\CMS\Core\Context\Context;
use TYPO3\CMS\Frontend\Controller\TypoScriptFrontendController;
use Pixelant\Webkitpdf\Exceptions\NotAllowedHostException;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Class PdfUtility
 * @package Pixelant\Webkitpdf\Utility
 */
class PdfUtility
{
    /**
     * Plugin configuration
     *
     * @var array
     */
    protected static $configuration;

    /**
     * Get plugin configuration
     *
     * @return array
     */
    public static function getConfiguration(): array
    {
        if (self::$configuration === null) {
            if (version_compare(TYPO3_version, '9.0', '<')) {
                self::$configuration = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['webkitpdf'] ?? '');
            } else {
                self::$configuration = GeneralUtility::makeInstance(ExtensionConfiguration::class)->get('webkitpdf');
            }
            if (!is_array(self::$configuration)) {
                self::$configuration = [];
            }
            if (TYPO3_MODE === 'FE') {
                $feSettings = GeneralUtility::removeDotsFromTS(
                    self::getTSFE()->tmpl->setup['plugin.']['tx_webkitpdf_pdf.']['settings.'] ?? []
                );

                self::$configuration += $feSettings;
            }
        }

        return self::$configuration;
    }

    /**
     * Escapes a URI resource name so it can safely be used on the command line.
     *
     * @param string $inputName URI resource name to safeguard, must not be empty
     * @return  string  $inputName escaped as needed
     */
    public static function wrapUriName(string $inputName): string
    {
        return escapeshellarg($inputName);
    }

    /**
     * Checks if the given URL's host matches the current host
     * and sanitizes the URL to be used on command line.
     *
     * @param string $url The URL to be sanitized
     * @param array $allowedHosts
     * @return string  The sanitized URL
     * @throws NotAllowedHostException
     */
    public static function sanitizeURL(string $url, array $allowedHosts = []): string
    {
        //Make sure that host of the URL matches TYPO3 host or one of allowed hosts set in TypoScript.
        $parts = parse_url($url);
        if (!isset($parts['host']) || $parts['host'] !== GeneralUtility::getIndpEnv('TYPO3_HOST_ONLY')) {
            if (!in_array($parts['host'], $allowedHosts)) {
                throw new NotAllowedHostException($parts['host']);
            }
        }

        return self::wrapUriName($url);
    }

    /**
     * Decode url given as parameter
     *
     * @param string $url
     * @return string
     */
    public static function decodeUrl(string $url): string
    {
        return html_entity_decode(urldecode($url));
    }

    /**
     * Appends information about the FE user session to the URL.
     * This is used to be able to generate PDFs of access restricted pages.
     *
     * @param string $url The URL to append the parameters to
     * @return  string  The processed URL
     */
    public static function appendFESessionInfoToURL(string $url): string
    {
        if (strpos($url, '?') !== false) {
            $url .= '&';
        } else {
            $url .= '?';
        }
        $sessionId = self::getTSFE()->fe_user->getSessionId();

        $url .= 'FE_SESSION_KEY=' .
            rawurlencode(
                $sessionId .
                '-' .
                md5(
                    $sessionId .
                    '/' .
                    $GLOBALS['TYPO3_CONF_VARS']['SYS']['encryptionKey']
                )
            );
        return $url;
    }

    /**
     * Writes log messages to devLog
     *
     * Acts as a wrapper for t3lib_div::devLog()
     * Additionally checks if debug was activated
     *
     * @param string $title : title of the event
     * @param int $severity : severity of the debug event
     * @param array $dataVar : additional data
     * @return    void
     */
    public static function debugLogging($title, $severity = -1, $dataVar = [])
    {
        if (self::getConfiguration()['debug'] === '1') {
            GeneralUtility::devlog($title, 'webkitpdf', $severity, $dataVar);
        }
    }

    /**
     * Makes sure that given path has a slash as first and last character
     *
     * @param string $path : The path to be sanitized
     * @return string
     */
    public static function sanitizePath(string $path): string
    {
        return rtrim($path, '/') . '/';
    }


    /**
     * Lower than this time = expire
     *
     * @return int
     */
    public static function getCacheExpireTime()
    {
        $minutes = self::getConfiguration()['cacheThreshold'];

        return time() - $minutes * 60;
    }

    /**
     * @return TypoScriptFrontendController
     */
    public static function getTSFE()
    {
        return $GLOBALS['TSFE'];
    }

    /**
     * Check if FE user is logged in
     *
     * @return bool
     * @throws \TYPO3\CMS\Core\Context\Exception\AspectNotFoundException
     */
    public static function isFrontendLoggin(): bool
    {
        if (version_compare(TYPO3_version, '9.0', '<')) {
            return static::getTSFE()->loginUser;
        } else {
            $context = GeneralUtility::makeInstance(Context::class);
            return $context->getPropertyFromAspect('frontend.user', 'isLoggedIn', false);
        }
    }
}
