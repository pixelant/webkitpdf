<?php

namespace Pixelant\Webkitpdf\Tests\Utility;

use Nimut\TestingFramework\MockObject\AccessibleMockObjectInterface;
use Nimut\TestingFramework\TestCase\UnitTestCase;
use PHPUnit_Framework_MockObject_MockObject;
use Pixelant\Webkitpdf\Exceptions\NotAllowedHostException;
use Pixelant\Webkitpdf\Utility\PdfUtility;
use TYPO3\CMS\Frontend\Authentication\FrontendUserAuthentication;
use TYPO3\CMS\Frontend\Controller\TypoScriptFrontendController;

/**
 * Class PdfUtilityTest
 * @package Pixelant\Webkitpdf\Tests\Utility
 */
class PdfUtilityTest extends UnitTestCase
{
    /**
     * @var TypoScriptFrontendController|PHPUnit_Framework_MockObject_MockObject|AccessibleMockObjectInterface
     */
    protected $tsfe;

    /**
     * setup
     */
    public function setUp()
    {
        // Set extension settings
        $GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['webkitpdf'] = serialize($this->getExtConfiguration());


        $this->tsfe = $this->getAccessibleMock(TypoScriptFrontendController::class, [], [], '', false);

        $GLOBALS['TSFE'] = $this->tsfe;
    }

    /**
     * @test
     */
    public function getTSFEReturnTypoScriptFrontendController()
    {
        $this->assertSame(
            $this->tsfe,
            PdfUtility::getTSFE()
        );
    }

    /**
     * @test
     */
    public function getPluginSettingsReturnExtConfiguration()
    {
        $this->assertEquals(
            $this->getExtConfiguration(),
            PdfUtility::getConfiguration()
        );
    }

    /**
     * @test
     */
    public function wrapUriNameDoEscapeshellarg()
    {
        $name = 'ls /var/mnt';

        $this->assertEquals(
            escapeshellarg($name),
            PdfUtility::wrapUriName($name)
        );
    }

    /**
     * @test
     */
    public function notAllowedHostThrowsException()
    {
        $this->expectException(NotAllowedHostException::class);

        PdfUtility::sanitizeURL(
            'http://test.com/test',
            ['www.allowed.com']
        );
    }

    /**
     * @test
     */
    public function sanitizePathEndsWithSlashAtTheEnd()
    {
        $test = '/var/mnt';

        $this->assertStringEndsWith(
            '/',
            PdfUtility::sanitizePath($test)
        );
    }

    /**
     * @test
     */
    public function appendUrlNoParamsWithFrontendUserSession()
    {
        $sessionId = md5('test');
        $url = 'http://test.com/test';

        $feUser = $this->getAccessibleMock(FrontendUserAuthentication::class, ['getSessionId']);

        $feUser->expects($this->once())->method('getSessionId')->willReturn($sessionId);

        $this->tsfe->fe_user = $feUser;

        $this->assertEquals(
            $url . '?' . $this->getSessionId($sessionId),
            PdfUtility::appendFESessionInfoToURL($url)
        );
    }

    /**
     * @test
     */
    public function appendUrlWithParamsWithFrontendUserSession()
    {
        $sessionId = md5('test');
        $url = 'http://test.com/test?test=123';

        $feUser = $this->getAccessibleMock(FrontendUserAuthentication::class, ['getSessionId']);

        $feUser->expects($this->once())->method('getSessionId')->willReturn($sessionId);

        $this->tsfe->fe_user = $feUser;

        $this->assertEquals(
            $url . '&' . $this->getSessionId($sessionId),
            PdfUtility::appendFESessionInfoToURL($url)
        );
    }

    /**
     * @test
     */
    public function getPluginSettingsReturnCachedExtConfiguration()
    {
        $newSettings = ['test' => 123];
        $cacheSettings = PdfUtility::getConfiguration();

        $GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['webkitpdf'] = serialize($newSettings);

        $this->assertNotEquals(
            $newSettings,
            $cacheSettings
        );
    }

    /**
     * @test
     */
    public function decodeUrlWillDecodeUrlFromUrl()
    {
        $testUrl = 'http://test.local/product/?type=34298700&amp;tx_pxaproductmanager_pdf%5Bproduct%5D=6&amp;tx_pxaproductmanager_pdf%5Baction%5D=product&amp;tx_pxaproductmanager_pdf%5Bcontroller%5D=Pdf&amp;cHash=a66fa199d013ef0cf97c302e39066eeb';
        $expect = 'http://test.local/product/?type=34298700&tx_pxaproductmanager_pdf[product]=6&tx_pxaproductmanager_pdf[action]=product&tx_pxaproductmanager_pdf[controller]=Pdf&cHash=a66fa199d013ef0cf97c302e39066eeb';

        $this->assertEquals($expect, PdfUtility::decodeUrl($testUrl));
    }

    /**
     * Extension settings
     *
     * @return array
     */
    protected function getExtConfiguration()
    {
        return [
            'cacheThreshold' => 10,
            'disableCache' => 0
        ];
    }

    /**
     * @param $sessionId
     * @return string
     */
    protected function getSessionId($sessionId)
    {
        return 'FE_SESSION_KEY=' .
            rawurlencode(
                $sessionId .
                '-' .
                md5(
                    $sessionId .
                    '/' .
                    $GLOBALS['TYPO3_CONF_VARS']['SYS']['encryptionKey']
                )
            );
    }

    public function tearDown()
    {
        unset($GLOBALS['TSFE']);
        unset($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['webkitpdf']);

        $reflection = new \ReflectionProperty(PdfUtility::class, 'configuration');
        $reflection->setAccessible(true);
        $reflection->setValue(null, null);
    }
}
